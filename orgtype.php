<?php
function getOrgType($word){
    $word = str_replace('.','',$word);
    $word=mb_convert_case($word, MB_CASE_UPPER, "UTF-8");
    $word = str_replace('Ά','Α',$word);
    $word = str_replace('Έ','Ε',$word);
    $word = str_replace('Ή','Η',$word);
    $word = str_replace('Ί','Ι',$word); 
    $word = str_replace('Ύ','Υ',$word); 
    $word = str_replace('Ό','Ο',$word);
    $word = str_replace('Ώ','Ω',$word);
    $word = str_replace('KAI ','',$word);
    $orgTypes=array();
    $return='';
    $wordlength=mb_strlen($word,'UTF-8');
    $lexiconFile='orgtypes.csv';
    if (($handle = fopen($lexiconFile, "r")) !== FALSE){
        while (($data = fgetcsv($handle, 1000000, ",")) !== FALSE){
            $newdata =  array (
                'ORG_GR'=>$data[0], //cannot be first, can be last
                'ORG_ENG'=>$data[1], //cannot be first, can be last
                'ORG_FULL_1'=>$data[2], //can be anywhere
                'ORG_FULL_2'=>$data[3], //can be anywhere
                'ORG_FULL_ENG_1'=>$data[4],//can be anywhere
                'CLUSTER' =>$data[5],                
            );
         $orgTypes[]=$newdata;             
        }
         
    }
    fclose($handle);
    foreach($orgTypes as $typeRows){
        
        if ( (inOrgType($word,$typeRows['ORG_GR']) !== 'False') 
                ||( inOrgType($word,$typeRows['ORG_ENG']) !== 'False') 
                ||( inOrgType($word,$typeRows['ORG_FULL_1']) !== 'False') 
                ||( inOrgType($word,$typeRows['ORG_FULL_2']) !== 'False')  
                ||( inOrgType($word,$typeRows['ORG_FULL_ENG_1']) !== 'False')){    
            $return=$typeRows['CLUSTER'];
        }
    }
   
    return  $return;
}
function inOrgType($word,$field){
    $result='False';
    $wordlength=mb_strlen($word,'UTF-8');
    $typeRowsLength= mb_strlen($field,'UTF-8');
    $orgPos=mb_strpos($word,$field,0,'UTF-8');
    $prevPos=$orgPos-1;
    $nextPos=$orgPos+$typeRowsLength;
    $prevChar=mb_substr($word,$prevPos,1,'UTF-8');
    $nextChar=mb_substr($word,$nextPos,1,'UTF-8');
    if   (mb_strpos($word,$field) !== false){
         if (($prevChar===' ') ||  ($prevPos<0)){
             if (($nextChar===' ') || ($nextPos>=$wordlength)){
                 $result='True';
             }
             else{
                 $result='False';
             }
         }
         else{
             $result='False';
         }
    }
    else{
        $result='False';
    }
    return $result;
}