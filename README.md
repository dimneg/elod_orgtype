# README #


Version 1.0
Algorithm, php implementation of extracting Organization Type from Organization Name

Overview:

By using a table in which possible appearences of the organization type are projected we check if 
the string given can result to an organization type.
Table comes from https://docs.google.com/spreadsheets/d/19ezrMiUJSPxByn2Yb_lYEJ4LHGvSvNC6zgFW9o9DHew/edit#gid=0
which may be updated in the future. 
We extract is as a csv file and use it in our code

Algorithm:


-Remove punctuation from keyword 
-Convert case to capital
-Remove intonation from  Greek letters

-Read orgtypes.csv and create array. There are "source columns" that contain possible appearences and
a result column that becomes the output. (a second result may be used in the future depending on the use case)

-For every line of the array:
		-check if the value of any source columns is contained in the string (function inOrgType)
		(to do that we apply two rules:
		 -previous character must be empty or orgtype is found on the beginning of the string
		 -next character is empty or orgtype is found on the end of the string
		)
		
-Return the result or ''.

Php Usage:

Install php and run Php index.php

example:

$string='ΤΡΑΠΕΖΑ ΠΕΙΡΑΙΩΣ ΑΕ';
echo getOrgType($string); //ΑΕ





